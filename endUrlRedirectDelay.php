<?php
/**
 * endUrlRedirectDelay
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017 Denis Chenu <https://www.sondages.pro>
 * @copyright 2017 Dialogs <https://dialogs.ca>
 * @license AGPL
 * @version 0.0.2
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class endUrlRedirectDelay extends PluginBase
{

    protected $storage = 'DbStorage';

    static protected $name = 'endUrlRedirectDelay';
    static protected $description = 'redirect to the end url but after a delay';

    protected $settings = array(
        'delayRedirect' => array(
            'type' => 'int',
            'label' => 'Default delay for redirection (0 or empty deactivate the redirection)',
            'default' => "",
        ),
    );
    public function init() {
    $this->subscribe('beforeSurveySettings');
    $this->subscribe('newSurveySettings');
    $this->subscribe('afterSurveyComplete');
    //$this->subscribe('afterSurveyQuota');
    $this->subscribe('afterPluginLoad');
    }

    public function beforeSurveySettings() {
        $event = $this->event;
        $event->set("surveysettings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array(
                'delayRedirect'=>array(
                    'type'=>'int',
                    'label'=>$this->_translate('Delay for redirection'),
                    'help'=>$this->_translate('0 or empty deactivate the redirection'),
                    'current' => $this->get('delayRedirect', 'Survey', $event->get('survey'),$this->get('delayRedirect',null,null,$this->settings['delayRedirect']['default'])),
                ),
                'urlRedirect'=>array(
                    'type'=>'string',
                    'label'=>$this->_translate('Url for redirection'),
                    'help'=>$this->_translate('By default use the End URL'),
                    'current' => $this->get('urlRedirect', 'Survey', $event->get('survey'),$this->get('delayRedirect',null,null,"")),
                ),
                'urlRedirectSelf'=>array(
                    'type'=>'boolean',
                    'label'=>$this->_translate('Redirect to start new current survey'),
                    'help'=>$this->_translate('This setting have priority'),
                    'current' => $this->get('urlRedirectSelf', 'Survey', $event->get('survey'),$this->get('urlRedirectSelf',null,null,0)),
                ),
            )
        ));
    }

    public function newSurveySettings() {
        $event = $this->event;
        foreach ($event->get('settings') as $name => $value)
        {
            /* In order use survey setting, if not set, use global, if not set use default */
            $default=$event->get($name,null,null,isset($this->settings[$name]['default'])?$this->settings[$name]['default']:null);
            $this->set($name, $value, 'Survey', $event->get('survey'),$default);
        }
    }

    public function afterSurveyComplete() {
        $oEvent = $this->event;
        $surveyId = $oEvent->get("surveyId");
        $delayRedirect = intval($this->get('delayRedirect', 'Survey', $surveyId,$this->get('delayRedirect',null,null,$this->settings['delayRedirect']['default'])));
        if($delayRedirect > 0) {
            $delayRedirect = $delayRedirect*1000;
            $urlRedirect = trim($this->get('urlRedirect', 'Survey', $surveyId,$this->get('urlRedirect',null,null,'')));
            if($this->get('urlRedirectSelf', 'Survey', $surveyId,$this->get('urlRedirectSelf',null,null,0))) {
                $urlRedirect = $this->api->createUrl("survey/index",array('sid'=>$surveyId,'newtest'=>'Y'));
            }
            if(!$urlRedirect) {
                $oSurveyLanguageSetting = SurveyLanguageSetting::model()->findByPk(array("surveyls_survey_id"=>$surveyId,"surveyls_language"=>App()->getLanguage()));
                if(empty($oSurveyLanguageSetting) || empty($oSurveyLanguageSetting->surveyls_url)) {
                    return;
                }
                $urlRedirect = trim($oSurveyLanguageSetting->surveyls_url);
            }
            $urlRedirectScript = "setTimeout(function() { window.open('".$urlRedirect."', '_top'); }, ".$delayRedirect.");";
            App()->getClientScript()->registerScript("urlRedirectScript",$urlRedirectScript,CClientScript::POS_END);
        }
    }
    /**
     * get translation
     * @param string
     * @return string
     */
    private function _translate($string){
        return Yii::t('',$string,array(),get_class($this));
    }
    /**
     * Add this translation just after loaded all plugins
     * @see event afterPluginLoad
     */
    public function afterPluginLoad(){
        // messageSource for this plugin:
        $messageSource=array(
            'class' => 'CGettextMessageSource',
            'cacheID' => get_class($this).'Lang',
            'cachingDuration'=>3600,
            'forceTranslation' => true,
            'useMoFile' => true,
            'basePath' => __DIR__ . DIRECTORY_SEPARATOR.'locale',
            'catalog'=>'messages',// default from Yii
        );
        Yii::app()->setComponent(get_class($this),$messageSource);
    }

}
